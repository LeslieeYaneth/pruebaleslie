//
//  DetailMoviePresenter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 16/02/21.
//

import UIKit

protocol DetailMoviePresentationLogic {
}

class DetailMoviePresenter: DetailMoviePresentationLogic {
    // MARK: - Properties
    weak var viewController: DetailMovieDisplayLogic?

    // MARK: - Presentation Logic
}
