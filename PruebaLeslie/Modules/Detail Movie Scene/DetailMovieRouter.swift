//
//  DetailMovieRouter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 16/02/21.
//

import UIKit

protocol DetailMovieRoutingLogic {
}

protocol DetailMovieDataPassing {
    var dataStore: DetailMovieDataStore? { get }
}

class DetailMovieRouter: NSObject, DetailMovieRoutingLogic, DetailMovieDataPassing {
    // MARK: Properties
    weak var viewController: DetailMovieViewController?
    var dataStore: DetailMovieDataStore?
    // MARK: Routing Logic
}
