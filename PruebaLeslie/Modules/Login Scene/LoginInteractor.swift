//
//  LoginInteractor.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import PromiseKit
import Disk

protocol LoginBusinessLogic {
    func requestActiveOrders(user: String, password: String)
}

protocol LoginDataStore {
}

class LoginInteractor: LoginBusinessLogic, LoginDataStore {

    // MARK: - Properties
    var presenter: LoginPresentationLogic?
    private let backQueue = DispatchQueue.global(qos: .userInitiated)
    private let worker = UserWorker()

    // MARK: - Business Logic
    func requestActiveOrders(user: String, password: String) {
        let params: [String: Any] = [
            "country_code": "MX",
            "username": user,
            "password": password,
            "grant_type": "password",
            "client_id": "IATestCandidate",
            "client_secret": "c840457e777b4fee9b5 10fbcd4985b68"
        ]
        worker.loginUser(params: params).done { json in
            Token.Access_token = json["access_token"].stringValue
            Token.Token_type = json["token_type"].stringValue
            self.presenter?.presentLogin()
        }.catch { error in
            self.presenter?.presentLoginError()
        }
    }
}
