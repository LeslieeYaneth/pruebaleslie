//
//  MainFeedUseCases.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON

enum MainFeed {
    struct GetUserData {
        struct Response {
            let user: CurrentUser!
            let error: Bool
            let errorMsg: String
        }

        struct ViewModel {
            let user: CurrentUser!
        }
    }
}
