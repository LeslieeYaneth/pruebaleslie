//
//  MainFeedViewController.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON
import Foundation

protocol MainFeedDisplayLogic: class {
    func displayUserData(_ response: MainFeed.GetUserData.ViewModel)
    func displayUserDataError(_ response: MainFeed.GetUserData.Response)
}

class MainFeedViewController: BaseViewController {
    // MARK: - Properties
    internal var interactor: (MainFeedBusinessLogic & MainFeedDataStore)?
    internal var router: (NSObjectProtocol & MainFeedRoutingLogic & MainFeedDataPassing)?
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var cardLbl: UILabel!

    // MARK: - Setup
    override func setup() {
        let viewController = self
        let interactor = MainFeedInteractor()
        let presenter = MainFeedPresenter()
        let router = MainFeedRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showData()
        if UserProfile.UserProfile == nil {
            interactor!.requestInfo()
        } else {
            showUser()
        }
    }

    func showData() {
        showShadow(view: containerView)
    }

    func showUser() {
        let resURL = PruebaURLS.movieImagenURL
        userImg.kf.setImage(with: (resURL + UserProfile.UserProfile.profile_picture).convertStringToURL())
        nameLbl.text = UserProfile.UserProfile.name
        emailLbl.text = UserProfile.UserProfile.email
        cardLbl.text = UserProfile.UserProfile.card_number
    }

    private func showShadow(view: UIView) {
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 1
    }
}

// MARK: - Display Logic Methods
extension MainFeedViewController: MainFeedDisplayLogic {
    func displayUserData(_ response: MainFeed.GetUserData.ViewModel) {
        UserProfile.UserProfile = response.user
        showUser()
    }

    func displayUserDataError(_ response: MainFeed.GetUserData.Response) {
        let alert = UIAlertController(title: "Atención", message: response.errorMsg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
