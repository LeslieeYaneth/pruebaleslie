//
//  MainBaseRouter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

@objc protocol MainBaseRoutingLogic {
    func routeToMain()
    func routeToBoard()
}

class MainBaseRouter: NSObject, MainBaseRoutingLogic {
    // MARK: - Properties
    weak var viewController: MainBaseViewController?
    weak var mainView: MainFeedViewController!
    weak var boardView: BoardViewController!

    // MARK: Routing Logic
    func routeToMain() {
        guard let viewC = viewController else { return }
        mainView = UIStoryboard.main.instantiateViewController(withIdentifier: "MainFeedVC") as? MainFeedViewController
        mainView.view.frame = viewC.container.bounds
        viewC.addChild(mainView)
        viewC.container.addSubview(mainView.view)
        mainView.didMove(toParent: viewC)
    }

    func routeToBoard() {
        guard let viewC = viewController else { return }
        boardView = UIStoryboard.main.instantiateViewController(withIdentifier: "BoardVC") as? BoardViewController
        boardView.view.frame = viewC.container.bounds
        viewC.addChild(boardView)
        viewC.container.addSubview(boardView.view)
        boardView.didMove(toParent: viewC)
    }
}

