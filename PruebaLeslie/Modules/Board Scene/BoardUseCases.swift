//
//  BoardUseCases.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON

enum Board {
    struct GetMovies {
        struct Response {
            let movies: [MovieInfo]!
            let error: Bool
            let errorMsg: String
        }

        struct ViewModel {
            let movies: [MovieInfo]!
        }
    }
}
