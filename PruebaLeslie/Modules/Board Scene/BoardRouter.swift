//
//  BoardRouter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit
import SwiftyJSON

protocol BoardRoutingLogic {
    func routeToDetail()
}

protocol BoardDataPassing {
    var dataStore: BoardDataStore? { get }
}

class BoardRouter: NSObject, BoardRoutingLogic, BoardDataPassing {
    // MARK: Properties
    weak var viewController: BoardViewController?
    var dataStore: BoardDataStore?

    // MARK: Routing Logic
    func routeToDetail() {
        let viewC = viewController
        let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "DetailMovieVC") as! DetailMovieViewController
        vc.selectMovie = viewC?.selectMovie
        vc.modalPresentationStyle = .fullScreen
        viewC!.present(vc, animated: true, completion: nil)
    }
}

