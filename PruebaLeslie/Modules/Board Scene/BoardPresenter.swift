//
//  BoardPresenter.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

protocol BoardPresentationLogic {
    func presentMovies(_ response: Board.GetMovies.Response)
}

class BoardPresenter: BoardPresentationLogic {

    // MARK: - Properties
    weak var viewController: BoardDisplayLogic?

    // MARK: - Presentation Logic
    func presentMovies(_ response: Board.GetMovies.Response) {
        if response.error {
            viewController?.displayMoviesError(response)
        } else {
            let viewModel = Board.GetMovies.ViewModel(movies: response.movies)
            viewController?.displayMovies(viewModel)
        }
    }
}

