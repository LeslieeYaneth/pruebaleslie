//
//  BoardInteractor.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import PromiseKit
import Disk

protocol BoardBusinessLogic {
    func requestMovies()
}

protocol BoardDataStore {
}

class BoardInteractor: BoardBusinessLogic, BoardDataStore {

    // MARK: Properties
    internal var presenter: BoardPresentationLogic?
    // MARK: - Interactor Logic
    private let backQueue = DispatchQueue.global(qos: .userInitiated)
    private let worker = UserWorker()

    // MARK: - Interactor Logic
    func requestMovies() {
        let params: [String: Any] = [
            "country_code": "MX",
            "cinemas": 61,
        ]
        worker.boardMovies(params: params).done { movies in
            let response = Board.GetMovies.Response(movies: movies, error: false, errorMsg: "")
            self.presenter?.presentMovies(response)
        }.catch { error in
            let response = Board.GetMovies.Response(movies: nil, error: true, errorMsg: "No fue posible consultar la cartelera.")
            self.presenter?.presentMovies(response)
        }
    }
}
