//
//  TabMenuMainViewCell.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

class TabMenuMainViewCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet weak var titleLbl: UILabel!
}
