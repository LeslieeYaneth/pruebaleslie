//
//  UIStoryboardExtensions.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import UIKit

extension UIStoryboard {
    class var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}
