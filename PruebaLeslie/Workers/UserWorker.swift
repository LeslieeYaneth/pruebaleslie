//
//  UserWorker.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//
import Alamofire
import PromiseKit
import SwiftyJSON

class UserWorker {
    // MARK: Properties
    private let backQueue = DispatchQueue.global(qos: .userInitiated)

    // MARK: - Public Methods
    func loginUser(params: [String: Any]) -> Promise<JSON> {
        let request = NetworkManager.request(endpoint: PruebaAPI.loginUser(parameters: params))
        return request.then(on: backQueue) { json in
            return Promise { seal in
                let msg = json["error_description"].stringValue
                if msg.isEmpty {
                    seal.reject(NSError(domain: "Login-Error", code: 401, userInfo: [NSLocalizedDescriptionKey: msg]))
                } else {
                    seal.fulfill(json)
                    
                }
            }
        }
    }

    func infoUser() -> Promise<CurrentUser> {
        let request = NetworkManager.request(endpoint: PruebaAPI.infoUser)
        return request.then(on: backQueue) { json in
            return Promise { seal in
                let msg = json["error_description"].stringValue
                if msg.isEmpty {
                    seal.reject(NSError(domain: "Info-Error", code: 401, userInfo: [NSLocalizedDescriptionKey: msg]))
                } else {
                    seal.fulfill(CurrentUser(json: json))
                }
            }
        }
    }

    func boardMovies(params: [String: Any]) -> Promise<[MovieInfo]> {
        let request = NetworkManager.request(endpoint: PruebaAPI.board(parameters: params))
        return request.then(on: backQueue) { json in
            return Promise { seal in
                var array = [MovieInfo]()
                for item in json["movies"].arrayValue {
                    let movie = MovieInfo(json: item)
                    array.append(movie)
                }
                seal.fulfill(array)
            }
        }
    }
}
