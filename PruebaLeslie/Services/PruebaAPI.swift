//
//  PruebaAPI.swift
//  PruebaLeslie
//
//  Created by Lesliee Yaneth on 15/02/21.
//

import Foundation
import Alamofire

enum PruebaAPI {
    case loginUser(parameters: [String: Any])
    case infoUser
    case board(parameters: [String: Any])
}

// MARK: - Endpoint Declaration
extension PruebaAPI: APIEndpoint {
    var baseURL: String! {
        switch self {

        default:
            return PruebaURLS.restURL
        }
    }

    var path: String! {
        switch self {
        case .loginUser:
            return "v2/oauth/token"

        case .infoUser:
            return "v1/members/profile?country_code=MX"

        case .board:
            return "v2/movies"
        }
    }

    var method: HTTPMethod! {
        switch self {

        case .loginUser:
            return .post

        default:
            return .get
        }
    }

    var headers: HTTPHeaders! {
        switch self {
        case .infoUser:
            let params = [
                "api_key": PruebaURLS.apiKey,
                "Authorization": Token.Token_type + " " + Token.Access_token
            ]
            return params

        default:
            return ["api_key": PruebaURLS.apiKey]
        }
    }

    var parameters: [String: Any]? {
        switch self {

        case .loginUser(let params):
            return params

        case .board(let params):
            return params

        default:
            return nil
        }
    }

    var parameterEncoding: ParameterEncoding! {
        return URLEncoding.default
    }
}
